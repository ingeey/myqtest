﻿using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class QuestionPool
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public Group Group { get; set; }
        public List<Question> Questions { get; set; }


    }
}
