﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class ExamResult
    {
        public int Id { get; set; }
        public int TotalPercentage { get; set; }
        public List<QuestionPoolResult> QuestionPoolResults { get; set; }
        public int Result { get; set; }
    }
}
