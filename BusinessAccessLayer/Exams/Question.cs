﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer
{
    public class Question
    {
        public int Id { get; set; }
        public String QuestionText { get; set; }
        public QuestionCategory QuestionCategory { get; set; }
        public QuestionType QuestionType { get; set; }
        public int DurationInMinutes { get; set; }
        public int RequiredPercentage { get; set; }
        public List<QuestionPool> QuestionPools { get; set; }
        public List<Answer> Answers { get; set; }
    }
}
