﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class QuestionCategory
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int PointsIfTrue { get; set; }
        public int PointsIfFalse { get; set; }
    }
}
