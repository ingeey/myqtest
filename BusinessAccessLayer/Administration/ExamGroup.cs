﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class ExamGroup
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public Group Group { get; set; }
        public List<Exam> Exams { get; set; }
        public List<Student> Students { get; set; }
        public int AllowedTakings { get; set; }
        public DateTime AllowedDateFrom { get; set; }
        public DateTime AllowedDateTo { get; set; }
    }
}
