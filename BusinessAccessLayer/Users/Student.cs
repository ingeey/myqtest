﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Student : User
    {
        public String JMBG { get; set; }
        public String Gender { get; set; }
        public String Email { get; set; }
        public String Adress { get; set; }
        public String City { get; set; }
        public String Phone { get; set; }
        public String Company { get; set; }
        public String Qualification { get; set; }
        public String Occupation { get; set; }
        public int ExamCredit { get; set; }
        public Group Group { get; set; }
        public ExamGroup ExamGroup { get; set; }
    }
}
