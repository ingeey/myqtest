﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class AnsweredQuestion
    {
        public int Id { get; set; }
        public ExamTaking ExamTaking { get; set; }
        public String QuestionText { get; set; }
        public Answer RequestedAnswer { get; set; }
        public Answer GivenAnswer { get; set; }
    }
}
