﻿using MyQTest.Models;
using MyQTest.ServiceLayer.Interfaces;
using MyQTest.ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyQTest.Controllers
{
    public class StudentController : Controller
    {
        IStudentService studentService = new StudentService();
        //
        // GET: /Student/

        public ActionResult Index()
        {
            List<Student> items = studentService.GetStudents();
            return View(items);
        }

        //
        // GET: /Student/Details/5

        public ActionResult Details(Student item)
        {
            return View(item);
        }

        //
        // GET: /Student/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Student/Create

        [HttpPost]
        public ActionResult Create(Student item)
        {
            if (!ModelState.IsValid)
            {
                return View("Create", item);
            }

            studentService.CreateStudent(item);

            return RedirectToAction("Index");
        }

        //
        // GET: /Student/Edit/5

        public ActionResult Edit(int id)
        {
            Student item = studentService.GetStudent(id);
            return View(item);
        }

        //
        // POST: /Student/Edit/5

        [HttpPost]
        public ActionResult Edit(Student item)
        {
            if (!ModelState.IsValid)
            {
                return View("Delete", item);
            }

            studentService.UpdateStudent(item);

            return RedirectToAction("Index");
        }

        //
        // GET: /Student/Delete/5

        public ActionResult Delete(int id)
        {
            Student item = studentService.GetStudent(id);
            return View(item);
        }

        //
        // POST: /Student/Delete/5

        [HttpPost]
        public ActionResult Delete(Student item)
        {
            if (!ModelState.IsValid)
            {
                return View("Delete", item);
            }

            studentService.RemoveStudent(item);

            return RedirectToAction("Index");
        }
    }
}
