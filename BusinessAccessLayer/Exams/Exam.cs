﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Exam
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public ExamGroup ExamGroup { get; set; }
        public List<QuestionPool> QuestionPools { get; set; }
        public CriteriaType CriteriaType { get; set; }
    }
}
