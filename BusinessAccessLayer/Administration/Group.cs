﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Group
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public Organization Organization { get; set; }
        public List<Student> Students { get; set; }
        public List<Administrator> Administrators { get; set; }
        public List<QuestionPool> QuestionPools { get; set; }

    }
}
