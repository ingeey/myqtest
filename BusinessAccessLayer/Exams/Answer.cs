﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Answer
    {
        public int Id { get; set; }
        public Boolean Status { get; set; }
        public String Text { get; set; }
    }
}
