﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQTest.Models
{
    public class HeadAdministrator : User
    {
        public List<Organization> Organizations { get; set; }
    }
}
