﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class Organization
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public List<Group> Groups { get; set; }
        public List<Administrator> Administrators { get; set; }
    }
}
