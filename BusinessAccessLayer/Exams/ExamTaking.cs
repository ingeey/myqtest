﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class ExamTaking
    {
        public int Id { get; set; }
        public Student Student { get; set; }
        public Exam Exam { get; set; }
        public int TakingNumber { get; set; }
        public DateTime TakingDate { get; set; }
        public List<AnsweredQuestion> AnsweredQuestions { get; set; }
        public ExamResult ExamResult { get; set; }
    }
}
