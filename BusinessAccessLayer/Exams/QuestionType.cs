﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class QuestionType
    {
        public int Id { get; set; }
        public AnswerControlType AnswerControlType { get; set; }
        public AnswerContentType AnswerContentType { get; set; }
    }
}
