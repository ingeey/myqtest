﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class QuestionPoolResult
    {
        public int Id { get; set; }
        public QuestionPool QuestionPool { get; set; }
        public int AnswersPercentage { get; set; }
        public int Result { get; set; }
    }
}
